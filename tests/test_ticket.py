import ticket
import py

#def testTicketSoldOut():
#    assert ticket.ticket_percentage_text(100) == "All tickets gone."

#def testTicketEarlyBirdSoldOut():
#    assert ticket.ticket_percentage_text(100, earlybird = True) == "All tickets gone."

@py.test.mark.parametrize(("pc", "earlybird", "expected"), [
    (100, False, "All tickets gone. FAIL"),

    # these two tests still run even though the above fails!
    (100, True, "All tickets gone."),
    (86, True, "86% earlybird sold."),])

def testTicket(pc, earlybird, expected):
    assert ticket.ticket_percentage_text(pc, earlybird) == expected

@py.test.mark.xfail
@py.test.mark.issue300
def testOverselling():
    """
    FIXME: this test demonstrates a bug: "Almost all tickets sold out."
    """
    assert ticket.ticket_percentage_text(101) == "All tickets gone."
